package service;

import java.util.List;

import model.Playlist;
import model.Song;

public interface SongService {
	
	public void addSongToPlaylist(Song song, Playlist playlist);
	public List<Song> getAllSongs();
	public List<Song> getSongFromPlaylistById(Integer id);
	void updateSongFromPlaylist(Song song, Playlist playlist);
	void deleteSongFromPlaylist(Song song, Playlist playlist);

}
