package service;

import java.util.List;

import model.Playlist;
import model.Song;

public interface PlaylistService {
	
	public void addPlaylist(Playlist playlist);
	public List<Playlist> getAllPlaylist();
	public Playlist getPlaylistById(Integer id);
	public Playlist getPlaylistByName(String title);
	void updatePlaylist(Playlist playlist);
	void deletePlaylist(Playlist playlist);
	
	public List<Song> getAllSongsFromPlaylist(Integer playlistId);
}
