package factory;

import dao.UserDAO;

public abstract class DAOFactory {

	public static final int JDBC = 1;
	public static final int JPA = 2;
	
	public abstract UserDAO getUserDAO();
	
	public static DAOFactory getDAOFactory(int whichFactory) {
		switch (whichFactory) {
			case JDBC :
				return new JDBCDAOFactory();
			case JPA :
				return new JPADAOFactory();
		}
		return null;
	}
}
