package factory;

import dao.UserDAO;
import dao.UserDAOImpl;

public class JPADAOFactory extends DAOFactory {

	@Override
	public UserDAO getUserDAO() {
		return new UserDAOImpl();
	}
}
