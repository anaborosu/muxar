package factory;

import persistanceJDBCService.UserDAOImpl;
import dao.UserDAO;

public class JDBCDAOFactory extends DAOFactory {

	@Override
	public UserDAO getUserDAO() {
		return new UserDAOImpl();
	}
}
