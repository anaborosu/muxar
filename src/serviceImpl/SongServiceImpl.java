package serviceImpl;

import java.util.ArrayList;
import java.util.List;

import model.Playlist;
import model.Song;
import service.PlaylistService;
import service.SongService;

public class SongServiceImpl implements SongService{

	private List<Song> songs = new ArrayList<Song>();
	private PlaylistService playlistService;
	private static SongServiceImpl instance;
	
	public static SongServiceImpl getInstance(){
		if(instance == null){
			instance = new SongServiceImpl();
		}
		
		return instance;
	}
	
	private SongServiceImpl(){
		songs = generateSongs();
		playlistService = PlaylistServiceImpl.getInstance();
	}
	
	private List<Song> generateSongs(){
		Song s1 = new Song(1, "Song1", "Album1", "S1", "1990");
		Song s2 = new Song(2, "Song2", "Album1", "S1", "1990");
		Song s3 = new Song(3, "Song3", "Album1", "S1", "1990");
		Song s4 = new Song(4, "Song4", "Album2", "S1", "1990");
		Song s5 = new Song(5, "Song5", "Album2", "S1", "1990");
		Song s6 = new Song(6, "Song6", "Album2", "S1", "1990");
		Song s7 = new Song(7, "Song7", "Album3", "S1", "1990");
		Song s8 = new Song(8, "Song8", "Album3", "S1", "1990");
		Song s9 = new Song(9, "Song9", "Album4", "S1", "1990");
		Song s10 = new Song(10, "Song10", "Album4", "S1", "1990");
		
		List<Song> songs = new ArrayList<Song>();
		songs.add(s1);
		songs.add(s2);
		songs.add(s3);
		songs.add(s4);
		songs.add(s5);
		songs.add(s6);
		songs.add(s7);
		songs.add(s8);
		songs.add(s9);
		songs.add(s10);
		
		return songs;
	}
	
	@Override
	public void addSongToPlaylist(Song song, Playlist playlist) {
		playlistService.getPlaylistById(playlist.getId()).getSongs().add(song);		
	}

	@Override
	public List<Song> getAllSongs() {
		return songs;
	}

	@Override
	public List<Song> getSongFromPlaylistById(Integer id) {
		return playlistService.getPlaylistById(id).getSongs();
	}

//	@Override
//	public Song getSongFromPlaylistByName(String title, Playlist playlist) {
//		List<Song> playlistSongs = playlistService.getPlaylistById(playlist.getId()).getSongs();
//		for(Song s : playlistSongs){
//			if(s.getName().equals(title)){
//				return s;
//			}
//		}
//		return null;
//	}

	@Override
	public void updateSongFromPlaylist(Song song, Playlist playlist) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteSongFromPlaylist(Song song, Playlist playlist) {
		// TODO Auto-generated method stub
		
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	
}
