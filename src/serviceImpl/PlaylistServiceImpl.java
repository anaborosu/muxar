package serviceImpl;

import java.util.ArrayList;
import java.util.List;

import model.Playlist;
import model.Song;
import service.PlaylistService;

public class PlaylistServiceImpl implements PlaylistService{

	private List<Playlist> playlists = new ArrayList<Playlist>();
	public static PlaylistServiceImpl instance;
	
	public static PlaylistServiceImpl getInstance(){
		if(instance == null){
			instance = new PlaylistServiceImpl();
		}
		
		return instance;
	}
	
	private PlaylistServiceImpl(){
		playlists = generatePlaylists();
	}
	
	private List<Song> generateSongs(){
		Song s1 = new Song(1, "Song1", "Album1", "S1", "1990");
		Song s2 = new Song(2, "Song2", "Album1", "S1", "1990");
		Song s3 = new Song(3, "Song3", "Album1", "S1", "1990");
		Song s4 = new Song(4, "Song4", "Album2", "S1", "1990");
		Song s5 = new Song(5, "Song5", "Album2", "S1", "1990");
		Song s6 = new Song(6, "Song6", "Album2", "S1", "1990");
		Song s7 = new Song(7, "Song7", "Album3", "S1", "1990");
		Song s8 = new Song(8, "Song8", "Album3", "S1", "1990");
		Song s9 = new Song(9, "Song9", "Album4", "S1", "1990");
		Song s10 = new Song(10, "Song10", "Album4", "S1", "1990");
		
		List<Song> songs = new ArrayList<Song>();
		songs.add(s1);
		songs.add(s2);
		songs.add(s3);
		songs.add(s4);
		songs.add(s5);
		songs.add(s6);
		songs.add(s7);
		songs.add(s8);
		songs.add(s9);
		songs.add(s10);
		
		return songs;
	}
	
	private List<Playlist> generatePlaylists(){
		List<Song> playlist1Songs = new ArrayList<Song>();
		playlist1Songs.add(generateSongs().get(0));
		playlist1Songs.add(generateSongs().get(2));
		playlist1Songs.add(generateSongs().get(3));
		playlist1Songs.add(generateSongs().get(6));
		Playlist p1 = new Playlist(1, "Playlist1", "Cel mai tare playlist", playlist1Songs);
		
		List<Song> playlist2Songs = new ArrayList<Song>();
		playlist2Songs.add(generateSongs().get(3));
		playlist2Songs.add(generateSongs().get(5));
		playlist2Songs.add(generateSongs().get(1));
		Playlist p2 = new Playlist(2, "Playlist2", "Cel mai slab playlist", playlist2Songs);
		
		List<Song> playlist3Songs = new ArrayList<Song>();
		playlist3Songs.add(generateSongs().get(8));
		playlist3Songs.add(generateSongs().get(7));
		playlist3Songs.add(generateSongs().get(4));
		playlist3Songs.add(generateSongs().get(2));
		Playlist p3 = new Playlist(3, "Playlist3", "Alt playlist", playlist3Songs);
		
		playlists.add(p1);
		playlists.add(p2);
		playlists.add(p3);
		
		return playlists;
	}
	
	@Override
	public void addPlaylist(Playlist playlist) {
		playlists.add(playlist);		
	}

	@Override
	public List<Playlist> getAllPlaylist() {
		return playlists;
	}

	@Override
	public Playlist getPlaylistById(Integer id) {
		return playlists.get(id - 1);
	}

	@Override
	public Playlist getPlaylistByName(String title) {
		for(Playlist p : playlists){
			if(p.getTitle().equalsIgnoreCase(title)){
				return p;
			}
		}
		return null;
	}

	@Override
	public void updatePlaylist(Playlist playlist) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePlaylist(Playlist playlist) {
		// TODO Auto-generated method stub
		
	}

	public List<Playlist> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(List<Playlist> playlists) {
		this.playlists = playlists;
	}

	@Override
	public List<Song> getAllSongsFromPlaylist(Integer playlistId) {
		return getPlaylistById(playlistId).getSongs();
	}

	
}
