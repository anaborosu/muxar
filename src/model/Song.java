package model;


public class Song {
	
	private Integer id;
	private String name;
	private String album;
	private String singer;
	private String year;
	
	public Song() {
	}

	public Song(Integer id, String name, String album, String singer, String year) {
		this.id = id;
		this.name = name;
		this.album = album;
		this.singer = singer;
		this.year = year;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	
}
