package model;

import java.util.List;

public class Playlist {
	
	private Integer id;
	private String title;
	private String description;
	private List<Song> songs;
	
	public Playlist() {
	}

	public Playlist(Integer id, String title, String description, List<Song> songs) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.songs = songs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	
}
