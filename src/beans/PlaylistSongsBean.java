package beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import service.SongService;
import serviceImpl.SongServiceImpl;
import model.Song;

@ManagedBean(name="playlistSongsBean")
@RequestScoped
public class PlaylistSongsBean {

	private List<Song> songs;
	private SongService songService;
	
	@PostConstruct
	public void init(){
		String playlistId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("playlist");
		
		songService = SongServiceImpl.getInstance();
		songs = songService.getSongFromPlaylistById(Integer.parseInt(playlistId));
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	
}
