package beans;

import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import factory.DAOFactory;

@RequestScoped
public class BeanUtils {

	public static DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);
	
	public static HttpSession getSession(){
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

}
