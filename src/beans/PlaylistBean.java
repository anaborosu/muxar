package beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.Playlist;
import model.Song;
import service.PlaylistService;
import serviceImpl.PlaylistServiceImpl;

@ManagedBean(name="playlistBean")
@RequestScoped
public class PlaylistBean{
	
	private Playlist playlist;
	private List<Playlist> playlists;
	private List<Song> playlistSongs;
	private PlaylistService playlistService;

	public PlaylistBean() throws SQLException{
		playlist = new Playlist(); 
		playlistService = PlaylistServiceImpl.getInstance();
		playlists = playlistService.getAllPlaylist();
		playlistSongs = new ArrayList<Song>();
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public List<Playlist> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(List<Playlist> playlists) {
		this.playlists = playlists;
	}
	
	public List<Song> getPlaylistSongs() {
		return playlistSongs;
	}

	public void setPlaylistSongs(List<Song> playlistSongs) {
		this.playlistSongs = playlistSongs;
	}

	public String createPlaylist(){
		playlistService.addPlaylist(playlist);
		
		return "playlistView.xhtml?faces-redirect=true";
	}
}
