package beans;

import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.User;
import dao.UserDAO;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean {
	
	private User user;
	private UserDAO userDAO;
		
	public LoginBean() {
		user = new User();
	}

	public void login() {
		userDAO = BeanUtils.daoFactory.getUserDAO();
		try {
			if(userDAO.getUser(user.getUsername(), user.getPassword()) != null){
				BeanUtils.getSession().setAttribute("user", user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
