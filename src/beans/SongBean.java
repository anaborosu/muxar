package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.Playlist;
import model.Song;
import service.PlaylistService;
import service.SongService;
import serviceImpl.PlaylistServiceImpl;
import serviceImpl.SongServiceImpl;

@ManagedBean(name="songBean")
@RequestScoped
public class SongBean{
	
	private Song song;
	private List<Song> songs;
	private SongService songService;
	private PlaylistService playlistService;
	
	public SongBean() {		
		song = new Song();
		songService = SongServiceImpl.getInstance();
		songs = songService.getAllSongs();
		playlistService = PlaylistServiceImpl.getInstance();
	}
	
	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	public Playlist getCurrentPlaylist(Integer id){
		return playlistService.getPlaylistById(id);
	}
	
	public String createSong(Integer id){
		songService.addSongToPlaylist(song, getCurrentPlaylist(id));
		
		return "playlistView.xhtml?faces-redirect=true";
	}
	
}
