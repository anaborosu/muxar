package dao;

import java.sql.SQLException;

import model.User;

public interface UserDAO {

	public void insertUser(User user) throws SQLException;	
	public User getUser(String username, String password) throws SQLException;
}
