package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AppPersistenceContext {

	private static EntityManagerFactory factory;
	
	private static EntityManager em;
	
	public static EntityManager createEntityManager(){
		factory = Persistence.createEntityManagerFactory("Muxar");
        em = factory.createEntityManager();
        
        return em;
	}

	public static EntityManagerFactory getFactory() {
		return factory;
	}

	public static EntityManager getEm() {
		return em;
	}

}
