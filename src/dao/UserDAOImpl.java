package dao;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.User;

public class UserDAOImpl implements UserDAO{

	private static EntityManagerFactory factory;
	private static EntityManager em;
	
	public UserDAOImpl() {
		factory = Persistence.createEntityManagerFactory("Muxar");
        em = factory.createEntityManager();
	}
	
	@Override
	public void insertUser(User user) throws SQLException {
		em.getTransaction().begin();	
		try {
			em.persist(user);
			em.getTransaction().commit();
		} catch (RuntimeException e) {
			em.getTransaction().rollback();
		}
		
	}

	@Override
	public User getUser(String username, String password) throws SQLException {
		Query q = em.createQuery("SELECT u FROM User u WHERE u.username = :username AND u.password = :password");
        q.setParameter("username", username);
        q.setParameter("password", password);
        
        return (User) q.getSingleResult();
	}

}
