package persistanceJDBCService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.UserDAO;
import model.User;

public class UserDAOImpl extends DBConnection implements UserDAO{

	@Override
	public void insertUser(User user) throws SQLException{
		Connection con = getConnection();
		con.createStatement().executeUpdate(
				"insert into userAccount (username, password, role) values "
				+ "('" + user.getUsername() + "', '" + user.getPassword() + "', '" + user.getRole() + "')");
		con.close();
	}
	
	@Override
	public User getUser(String username, String password) throws SQLException{
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(
		   "select * from userAccount where (username = '" + username + "' and password = '" + password + "')"); 
	
		//get user from database
		ResultSet result =  ps.executeQuery();
	
		User currentUser = null;
		while(result.next()){
			currentUser = new User(result.getString("username"), result.getString("password"), result.getString("role"));
		}
		
		con.close();
		return currentUser;
	}
}
